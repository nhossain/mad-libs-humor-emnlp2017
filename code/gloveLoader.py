# -*- coding: utf-8 -*-
"""
@author: Nabil Hossain
"""

import scipy.spatial.distance
import numpy as np

from nltk.corpus import stopwords
stoplist = set(stopwords.words('english'))

#import gensim
#glove_gensim_model = "/Users/Nabil/humor-datasets/glove/glove-gensim-master/glove.840B.300d.gensim"  # also 840B
#glove = gensim.models.KeyedVectors.load_word2vec_format(glove_gensim_model, binary=False)  # C text format


def similarity(v1,v2):
    return 1-scipy.spatial.distance.cosine(v1,v2)


def getVectorSimilarity(word, context, model,method = 0):
    if word not in model:
        return -100   # marker for "not found"
    cntx = context.split()
    words = [w.lower() for w in cntx if w.lower() in model and w.lower() not in stoplist]
    if words == []:
        return 0
    vec = [model[wrd] for wrd in words]
    if method == 0 and len(vec) > 1:
        context_vector = sum(vec)*1.0/len(vec)   
        return similarity(context_vector, model[word])    
    else:
        return sum([similarity(v, model[word]) for v in vec ])*1.0/len(vec)
        
        
glove = {}
with open('/Users/Nabil/humor-datasets/glove/glove-gensim-master/glove.840B.300d.gensim') as f:
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        glove[word] = coefs
    f.close()