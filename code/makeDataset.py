# -*- coding: utf-8 -*-
"""
@author: Nabil Hossain

This module extracts features from the data and saves them in a pickle file.
The classifier will directly use this pickled data.
"""

from __future__ import division
import json
import re
import numpy as np
import pickle
from ngrams import *  # used in calculating character n-gram probabilities

print("Loading GloVe vectors, will take about 20 mins...",)
from gloveLoader import *  # loads glove vectors, takes about 20 mins
print("completed")

def getGrade(grade):
    votes = sum([int(g) for g in grade])
    if votes > 5:   return 1     # use "vast majority" vote
    elif votes < 4: return -1
    else:           return 0
     
     
HINT_NORMALIZER = {'nns':'nn','vbg':'vb','vbd':'vb','vbn':'vb','vbz':'vb'}
def mapHint(h):    
    # make sure to remove any '_plural' tags, if present
    return HINT_NORMALIZER[h] if h in HINT_NORMALIZER else h.split('_')[0]  
    
    
def getWordLengthScore(word):
    if len(word) > 2:
        probs = [log10(P3l(g)) for g in ngrams(word, 3)]
    elif len(word) == 2:
        probs = [log10(P2l(g)) for g in ngrams(word, 2)]
    else:
        print "ERROR - word must be 2 or more characters long: ", word
    return np.mean(probs), min(probs), max(probs)
    

def getContextSentence(text, blankIdx):
    # find the sentence which contains the blanks
    sentences = text.split('\n')
    blankCounter = -1
    for i in range(len(sentences)):
        blankCounter += len(re.findall('<.+?/>',sentences[i]))
        if blankCounter >= blankIdx:
            return sentences[i]
    raise ValueError("Didnt find context sentence")


def getFeature(text, blankIdx, word, hint, title):
    '''Note: missing 2 features related to language model word probability as 
       we dont have access to the language model (from Microsoft) anymore'''

    text = text.lower()
    text = re.sub('a/an','a',text)
    sentenceContext = removePunctuation(re.sub('<.+?/>','', getContextSentence(text, blankIdx)))
    storyContext = removePunctuation(re.sub('<.+?/>','',text))
    
    x = []
        
    # Word embedding features: distance from centriod of sentence, title, story
    x.append(getVectorSimilarity(word, title, glove))            #titleDist
    x.append(getVectorSimilarity(word, sentenceContext, glove))  #sentenceDist
    x.append(getVectorSimilarity(word, storyContext, glove))     #storyDist
        
    # hint type
    x.append(mapHint(hint))                                          
    
    # word length
    x.append(len(word))                                                 
    
    # letter trigram probability (avg, min, max)
    x += (getWordLengthScore(word))

    # add feature?: distance from original word that was in the blank    
   
    return x
    

def removePunctuation(line):   
    line = re.sub('\"', '\'', line)
    line = re.sub('\.', ' ', line)
    line = re.sub('\,', ' ', line)
    line = re.sub('\!', ' ', line)
    line = re.sub('\?', ' ', line)
    line = re.sub('\'', '', line)
    #line = re.sub('-, ' ', line)
    
    return line
    
               
def makeDataset(f = '../data/train-polichecked.json'):
    #f = '../data/test-FT-polichecked.json'
    #f = '../data/validation-polichecked.json'
    data = json.load(open(f))
    X = []
    y = []
    for title, v in data.items():
        replaced, hints = zip(*re.findall('<(.+?):+?([^:].+?)/>',v['text'].lower()))
        
        for version, info in v.items():
            if version == 'text': continue
            
            filledIn = info['filledIn']
            
            for i in range(len(filledIn)):
                if re.search('^redacted',filledIn[i]):  # some filledIn words were redacted based on Microsoft's censorship policies
                    continue
                
                label = getGrade(info['wordFunninessGrades'][i])
                if label != 0:
                    X.append(getFeature(v['text'], i, filledIn[i], hints[i], title))
                    y.append((label, title))
                
    return X,y
    
  
with open('../data/data.pkl','w') as f:
    pickle.dump(makeDataset('../data/train-polichecked.json'),f)
    pickle.dump(makeDataset('../data/validation-polichecked.json'),f)
    