# -*- coding: utf-8 -*-
"""
@author: Nabil Hossain
"""
  
import pickle
import numpy as np
from collections import Counter
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, f1_score, \
confusion_matrix, precision_score, recall_score, accuracy_score

#from sklearn.grid_search import GridSearchCV
#import pprint
#import operator

def chance(X, xvalid, y, yvalid):
    # chance classifier
    counter = Counter([v[0] for v in y])
    pred = counter.most_common()[0][0]
    print("Chance train accuracy = %.2f" % (counter[pred]*100/sum(counter.values())))
    
    test_counter = Counter([v[0] for v in yvalid])
    print("Chance test accuracy = %.2f" % (test_counter[pred]*100/sum(test_counter.values())))


def chanceHint(X, xvalid, y, yvalid):
    # chance classifier based on hint type:
        # Each hint type is predicted as the output label which had the biggest percentage for the hint type in the training data
    qtrn = [x[3] for x in X]
    qtst = [x[3] for x in xvalid]

    qtrnDic = {}
    count = {}
    for i in range(len(qtrn)):
        key = int(qtrn[i])
        if y[i][0] == 1:            
            qtrnDic[key] = qtrnDic.setdefault(key,0) + y[i][0]
        count[key] = count.setdefault(key,0) + 1
    for k,v in qtrnDic.items():
        qtrnDic[k] = v*1.0/count[k]
    #pprint(sorted(qtrnDic.items(),key=operator.itemgetter(1),reverse=True))

    trainpred = []
    for i in range(len(qtrn)):
        key = int(qtrn[i])
        if qtrnDic[key] >= 0.5:
            trainpred.append(1)
        else:
            trainpred.append(-1)
    print("Train Accuracy: %.3f" % accuracy_score([v[0] for v in y], trainpred))
    
    testpred = []
    for i in range(len(qtst)):
        key = int(qtst[i])
        if qtrnDic[key] >= 0.5:
            testpred.append(1)
        else:
            testpred.append(-1)
    print("Test Accuracy: %.3f\n" % accuracy_score([v[0] for v in yvalid], testpred))
    
    
def train(titles, X, y, xvalid, yvalid):
      
    yvalid = [v[0] for v in yvalid]
    titles = list(set(i[1] for i in y))
    trainT, testT = cross_validation.train_test_split(titles, test_size=.2, random_state=0)
    
    xtrain = []; ytrain = []; xtest = []; ytest = []
    for i in range(len(y)):
        if y[i][1] in trainT:
            xtrain.append(X[i])
            ytrain.append(y[i][0])
        else:
            xtest.append(X[i])
            ytest.append(y[i][0])
            
    xtrain = np.array(xtrain)
    xtest = np.array(xtest)
            
    print("Train Count: %r, Test Count: %r" % (len(ytrain),len(ytest)))
    
    # Train Random Forest classifier        
    clf = RandomForestClassifier()
    
    ''' # cross validation (avoid for now):
    param_grid = {'n_estimators':[50,100,150,200], 'criterion':['gini'], 'min_samples_split':[10,15,20], 'min_samples_leaf':[5,10,15]}
    clf = GridSearchCV(RandomForestClassifier(), param_grid, cv=5, scoring = 'f1')
    '''
    clf.fit(xtrain,ytrain)


    isTrain = 1
    for dsetX in [xtrain,xtest]:
        if isTrain == 1:
            print"--------------------- TRAIN RESULTS -----------------------"
            dsetY=ytrain
            
        else:
            print"--------------------- TEST RESULTS ------------------------"
            dsetY=ytest
        #y_score = clf.decision_function(dsetX) # for use in calculating roc                  
        #print plot_ROC(dsetY, y_score, isTrain)

        pred = clf.predict(dsetX)    
        print "Confusion Matrix:" 
        print confusion_matrix(dsetY, pred)
    
        print "Precision (positive class): %.3f" % precision_score(dsetY,pred,average=None)[1]
        print "Recall (positive class): %.3f" % recall_score(dsetY,pred,average=None)[1]
    
        print "Precision (negative class): %.3f" % precision_score(dsetY,pred,average=None)[0]
        print "Recall (negative class): %.3f" % recall_score(dsetY,pred,average=None)[0]
        
        tree_performance = roc_auc_score(dsetY, pred)
        print 'Area under the ROC curve = {}'.format(tree_performance)
        
        print "F1: %.3f\n" % f1_score(dsetY, pred)
        print "Accuracy: %.3f\n" % accuracy_score(dsetY, pred) 
        if isTrain == 1:
            isTrain = 0
    
    '''
    # Train on full dataset with best parameters:
    clf2 = GridSearchCV(RandomForestClassifier(), clf.best_params_, scoring = 'f1')
    dsetX = np.append(xtrain,xtest,axis=0)
    dsetY = ytrain+ytest
    clf2.fit(dsetX,dsetY)
    '''

    if True:
        print"--------------------- FULL DATASET RESULTS ------------------------"
        pred = clf.predict(xvalid)    
        print "Confusion Matrix:" 
        print confusion_matrix(yvalid, pred)
    
        print "Precision (positive class): %.3f" % precision_score(yvalid,pred,average=None)[1]
        print "Recall (positive class): %.3f" % recall_score(yvalid,pred,average=None)[1]
    
        print "Precision (negative class): %.3f" % precision_score(yvalid,pred,average=None)[0]
        print "Recall (negative class): %.3f" % recall_score(yvalid,pred,average=None)[0]
        
        tree_performance = roc_auc_score(yvalid, pred)
        print 'Area under the ROC curve = {}'.format(tree_performance)
        
        print "F1: %.3f\n" % f1_score(yvalid, pred)
        print "Accuracy: %.3f\n" % accuracy_score(yvalid, pred) 
    
    #print clf.best_params_  
    return clf
    
    
with open('../data/data.pkl') as f:
    X, y = pickle.load(f)
    xvalid, yvalid = pickle.load(f)  


# NOTE sklearn cant handle categorical data 
HINT_MAPPER = {'nn': 0, u'vb': 1, u'jj': 2, u'body': 3, u'rb': 4, u'animal': 5, u'food': 6, u'liquid': 7}
for i in range(len(X)):
    X[i][3] = HINT_MAPPER[X[i][3]]

for i in range(len(xvalid)):
    xvalid[i][3] = HINT_MAPPER[xvalid[i][3]]
#TODO: the above introduces error as hints are now ordered. Need to fix this.
    
titles = list(set(i[1] for i in y))
clf = train(titles, X, y, xvalid, yvalid)

print("Classifier training complete. Saving classifier params in: ../madlibs-classifier.dat")
with open('../madlibs-classifier.dat','w') as f:
    pickle.dump(clf,f)
    