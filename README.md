# Classifier for Detecting Funny filled-in Words in Mad Libs
This is an implementation of the classifier in the following paper:

> Hossain, Nabil, John Krumm, Lucy Vanderwende, Eric Horvitz, and Henry Kautz. "[Filling the blanks (hint: plural noun) for mad libs humor.](http://aclweb.org/anthology/D/D17/D17-1067.pdf)" In Proceedings of the 2017 Conference on Empirical Methods in Natural Language Processing, pp. 638-647. 2017.

## Prerequisities
* Python 2.7
[//]: <> ([Gensim](http://radimrehurek.com/gensim/))
* [Scikit learn](http://scikit-learn.org/stable/install.html)
* [GloVe pretrained word embeddings](http://nlp.stanford.edu/projects/glove/) 
	* [[download link]](http://nlp.stanford.edu/data/glove.840B.300d.zip)
* [NLTK](http://www.nltk.org/install.html)
	* Only the stopwords package is required

## Train classifier
First extract dataset and features:
```
python makeDataset.py
```

Now train the classifier:
```
python train.py
```

### Notes
* The language model used in the original implementation is not accessible. As a result, we were not able to extract two of the features.
* Cross-validation has not been implemented. Adding it will yield better results.

